/**
 * RTE Page TS for extension default
 * @author Andreas Otto <andreas@otto-hanika.de>
 */

/**
 * Configure RTE settings
 */
RTE {
	default {
		contentCSS = EXT:twitterbootstrap/Resources/Public/bootstrap/css/bootstrap.min.css
		useCSS = 1
		showTagFreeClasses = 1
		enableWordClean = 1
		removeTrailingBR = 1
		removeComments = 1
		removeTagsAndContents = style,script
	}
}