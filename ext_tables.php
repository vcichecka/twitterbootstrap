<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Andreas Otto <andreas@otto-hanika.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Configuration of the default extension
 * @author Andreas Otto <andreas@otto-hanika.de>
 */
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

/**
 * Change tt_content TCA.
 */
t3lib_div::loadTCA("tt_content");

/**
 * Overwrite flexform for table content element to allow assignment of multiple CSS classes.
 */
$TCA['tt_content']['columns']['pi_flexform']['config']['ds']['*,table'] = 'FILE:EXT:twitterbootstrap/Configuration/Flexform/css_styled_content/flexform_ds.xml';

/**
 * Overwrite values of image columns.
 */
$TCA['tt_content']['columns']['imagecols']['config']['items'] = array(
	array('1','1'),
	array('2','2'),
	array('3','3'),
	array('4','4'),
	array('6','6'),
	array('12','12'),
);
?>
